# spring

A simple JS game engine.

Spring is currently not suitable for much more than experimentation. Code may completely break with new versions as things are shuffled around.
Many features are not yet supported, but it's super easy to write your own code to manipulate spring's canvas.

## Learn spring

There's some basic documentation for spring available [on the wiki](https://bitbucket.org/matthewd673/spring/wiki/Getting%20Started)